package nl.fhict.its3;

import nl.fhict.its3.config.CustomApplicationConfig;
import nl.fhict.its3.domain.Drink;
import nl.fhict.its3.repository.DrinkRepository;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class deploys CustomApplicationConfig on a Grizzly server
 */
class Publisher {

    private static final URI BASE_URI = URI.create("http://0.0.0.0:9090/cafe/");

    public static void main(String[] args) {
        init();

        try {
            CustomApplicationConfig customApplicationConfig = new CustomApplicationConfig();
            // create and start a grizzly server
            HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, customApplicationConfig, true);

            System.out.println("Hosting resources at " + BASE_URI.toURL());

            System.out.println("Try the following GET operations in your internet browser: ");
            String[] getOperations = {BASE_URI.toURL() + "drinks/tea", BASE_URI.toURL() + "drinks/coffee"};
            for (String getOperation : getOperations) {
                System.out.println(getOperation);
            }

        } catch (IOException ex) {
            Logger.getLogger(Publisher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void init(){
        DrinkRepository drinkRepository = new DrinkRepository();

        Drink drink = new Drink("Tea", "Hot", 200);
        Drink drink2 = new Drink("Espresso", "Hot", 50);

        drinkRepository.save(drink);
        drinkRepository.save(drink2);
        System.out.println(drinkRepository.count());

    }
}
