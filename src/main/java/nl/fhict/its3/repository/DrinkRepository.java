package nl.fhict.its3.repository;

import nl.fhict.its3.domain.Drink;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DrinkRepository {

    private static final Map<String, Drink> drinks = new HashMap<>();

    public long count(){ return drinks.size();};

    public Drink findByName(String name){
        return drinks.get(name);
    }

    public List<Drink> findAll(){
        return new ArrayList<Drink>(drinks.values());
    }

    public void delete(Drink drink){
        drinks.remove(drink.getName());
    }

    //new Drink
    public boolean save(Drink drink){
        if (drinks.containsKey(drink.getName())){
            return false;
        } else {
            drinks.put(drink.getName(), drink);
            return true;
        }
    }

}
