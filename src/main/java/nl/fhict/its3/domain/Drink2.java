package nl.fhict.its3.domain;

import lombok.Data;

@Data
public class Drink2 {
    private final String name;
    private final String type;
    private final int quantity;


}
