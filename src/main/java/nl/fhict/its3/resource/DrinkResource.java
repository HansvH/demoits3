package nl.fhict.its3.resource;

import nl.fhict.its3.domain.Drink;
import nl.fhict.its3.repository.DrinkRepository;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

@Path("/drinks")
public class DrinkResource {

    @Context
    private UriInfo uriInfo;

    @Context
    SecurityContext securityContext;

    @Inject
    private DrinkRepository drinkRepository;

    @GET
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllDrinks() {
        List<Drink> drinks;
        drinks = drinkRepository.findAll();
        GenericEntity<List<Drink>> entity = new GenericEntity<>(drinks) {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("/async")
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public void getAllDrinksAsync(@Suspended final AsyncResponse asyncResponse) {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Drink> drinks = drinkRepository.findAll();
                GenericEntity<List<Drink>> entity = new GenericEntity<>(drinks){};
                Response response = Response.ok(entity).build();
                asyncResponse.resume(response);
            }

        }).start();
    
    }

    @Path("/energy")
    @GET
    @RolesAllowed("student")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllEnergyDrinks(){
        List<Drink> drinks;
        drinks = drinkRepository.findAll();
        GenericEntity<List<Drink>> entity = new GenericEntity<>(drinks) {        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDrinkByName(@PathParam("name") String name){
        Drink drink = drinkRepository.findByName(name);
        if (drink == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Please provide a valid drink name.").build();
        } else {
            return Response.ok(drink).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createDrink(Drink drink){
        if(!drinkRepository.save(drink)){
            String entity = "Drink with drink name " + drink.getName() + " already exists.";
            return Response.status(Response.Status.CONFLICT).entity(entity).build();
        } else {
            String url = uriInfo.getAbsolutePath() + "/" + drink.getName(); // url of the created drink
            URI uri = URI.create(url);
            return Response.created(uri).build();
        }
    }



}
