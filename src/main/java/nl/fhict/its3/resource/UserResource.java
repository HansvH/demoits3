package nl.fhict.its3.resource;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

import java.security.Key;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import util.KeyGenerator;


@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    @Context
    private UriInfo uriInfo;

    @Inject
    private KeyGenerator keyGenerator;

    @POST
    @Path("/login")
    public Response authenticateUser(UserDTO user) {
        try {

            String login = user.getUsername();
            String password = user.getPassword();

            authenticate(login, password);

            String token = issueToken(login);

            return Response.ok().header(AUTHORIZATION, "Bearer " + token).build();

        } catch (Exception e) {
            return Response.status(Status.UNAUTHORIZED).entity(e.getMessage()).build();
        }

    }

    // For demo investigate production ready way to issue token
    private String issueToken(String login) {

        Key key = keyGenerator.generateKey();
        String jwtToken = Jwts.builder().setSubject(login).claim("role", "student")  //get role from DB
                .setIssuer(uriInfo.getAbsolutePath().toString()).setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS512, key).compact();

        return jwtToken;
    }

    private void authenticate(String login, String password) throws SecurityException {
        // DB: SELECT FORM DB WHERE USERNAME + ETC. DO SOME HASING ...
        Set<String> users = new HashSet<>(Arrays.asList("Hans", "Angel")); // for demo
        if (!users.contains(login))
            throw new SecurityException("Invalid user/password");

    }

}
