package nl.fhict.its3.config;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;

import nl.fhict.its3.security.AuthorizationFilter;
import nl.fhict.its3.security.CorsFilter;

import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomApplicationConfig extends ResourceConfig
{
    public CustomApplicationConfig()
    {
        packages("nl.fhict.its3.resource"); // find all resource endpoint classes in this package

        register(new CorsFilter());
        
        register(new ApplicationBinder());

        register(AuthorizationFilter.class);
        
        // log exchanged http messages
        register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME),
                Level.INFO, LoggingFeature.Verbosity.PAYLOAD_ANY, LoggingFeature.DEFAULT_MAX_ENTITY_SIZE));
    }
}
