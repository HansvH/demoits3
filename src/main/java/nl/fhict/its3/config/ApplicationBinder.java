package nl.fhict.its3.config;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

import nl.fhict.its3.repository.DrinkRepository;
import util.KeyGenerator;
import util.SimpleKeyGenerator;


public class ApplicationBinder extends AbstractBinder {
    @Override
    protected void configure() {

        bind(DrinkRepository.class).to(DrinkRepository.class);
        bind(SimpleKeyGenerator.class).to(KeyGenerator.class);


    }
}
